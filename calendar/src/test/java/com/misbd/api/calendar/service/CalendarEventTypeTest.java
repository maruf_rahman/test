/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.entity.CalendarEventType;
import com.misbd.api.calendar.repository.CalendarEventTypeRepository;
import com.misbd.api.calendar.request.CalendarEventTypeCreateRequest;
import com.misbd.api.calendar.request.CalendarEventTypeUpdateRequest;
import com.misbd.api.common.Message;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.Assert.*;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarEventTypeTest {
    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;
    @Autowired
    private CalendarEventTypeRepository calendarEventTypeRepository;

    public CalendarEventTypeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createCalendarEventType_Event1_Equal(){
        CalendarEventTypeCreateRequest calendarEventTypeCreateRequest = new CalendarEventTypeCreateRequest("Event 1", "Description 1");
        calendarYearConfigurationService.createCalendarEventType(calendarEventTypeCreateRequest);
        CalendarEventType calendarEventType = calendarEventTypeRepository.findByName("Event 1");
        assertEquals(calendarEventType.getDescription(),calendarEventTypeCreateRequest.getDescription());
    }
    
    @Test
    public void updateCalendarEventType_Event1_Equal(){
        CalendarEventTypeUpdateRequest calendarEventTypeUpdateRequest = new CalendarEventTypeUpdateRequest(1L, "Event 1", "Description 1 modified");
        calendarYearConfigurationService.updateCalendarEventType(calendarEventTypeUpdateRequest);
        CalendarEventType calendarEventType = calendarEventTypeRepository.findByName("Event 1");
        assertEquals(calendarEventType.getDescription(),calendarEventTypeUpdateRequest.getDescription());
    }
    @Test
    public void findCalendarEventTypeList_AllEvents_SizeEqual3(){
        assertEquals(3, calendarYearConfigurationService.findCalendarEventTypeList(0, 5).size());
    }
    @Test
    public void deleteCalendarEventType_Id3_Equal(){
        String message = calendarYearConfigurationService.deleteCalendarEventType(3L);
        assertEquals(message, Message.DELETED_MSG);
    }
}
