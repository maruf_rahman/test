/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ProBook
 */
public class GenerateCalendarTest {
    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;

    public GenerateCalendarTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void generateCalendar_Year2019_Day365() {
        calendarYearConfigurationService.generateCalendar(1L, 1001L, "2019");
    }
}
