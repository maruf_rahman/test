/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CalendarProfileAPITest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void createCalendarProfile_Profile1Template1_HttpStatusCreated() throws Exception {
        String json = "{"
                + "\"description\": \"Description 2\","
                + "\"name\": \"Profile 2\""
                + "}";
        mvc.perform(post("http://localhost:8080/api/v1/calendar-profiles")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isCreated());
    }
    
    @Test
    public void findCalenderProfile_ProfileListSize1_HttpStatusOk() throws Exception {
        mvc.perform(get("http://localhost:8080/api/v1/calendar-profiles?offset=0&limit=10")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
