/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.CalendarProfileRepository;
import com.misbd.api.calendar.request.CalendarProfileCreateRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarProfileTest {

    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;
    @Autowired
    private CalendarProfileRepository calendarProfileRepository;
    
    public CalendarProfileTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createCalendarProfile_Profile1Template1_NotNull() {
        calendarTemplateConfigurationService.createCaldendarProfile(new CalendarProfileCreateRequest("Profile 1", "template 1"));
        assertNotNull(calendarProfileRepository.findByName("Profile 1"));
    }

    @Test
    public void findCalenderProfile_ProfileListSize1_Equal() {
        assertEquals(1, calendarTemplateConfigurationService.findCalendarProfileList(0, 5).size());
    }
}
