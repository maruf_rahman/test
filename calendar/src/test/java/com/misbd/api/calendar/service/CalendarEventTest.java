/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.request.CalendarEventCreateRequest;
import com.misbd.api.common.CalendarUtils;
import com.misbd.api.common.Message;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarEventTest {
    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;
    
    @Test
    public void isEventPossible_EventTypeId1CalendarMasterId1Date_Equal() throws ParseException{
        List<Long> branchIds = new ArrayList<>();
        branchIds.add(1001L);
        assertEquals("",calendarYearConfigurationService.isEventPossible(1L,branchIds,CalendarUtils.pareStringToDateyyyyMMdd("2019-02-12"), 1L));
    }
    
    @Test
    public void createCalendarEvent_EventTypeId1CalendarMasterId1Date20190314_NotNull() throws ParseException{
        String message = calendarYearConfigurationService.createCalendarEvent(new CalendarEventCreateRequest(1L,1L,CalendarUtils.pareStringToDateyyyyMMdd("2019-03-14")));
        assertEquals(message, Message.SAVE_MSG);
    }
    
    @Test
    public void findCalendarEventList_CalendarMasterId1_SizeEqual4() throws ParseException{
        assertEquals(4,calendarYearConfigurationService.findCalendarEventList(1L, 0, 20).size());
    }
    
    @Test
    public void deleteCalendarEvent_Id7_Equal(){
        assertEquals(Message.DELETED_MSG,calendarYearConfigurationService.deleteCalendarEvent(7L));
    }
}
