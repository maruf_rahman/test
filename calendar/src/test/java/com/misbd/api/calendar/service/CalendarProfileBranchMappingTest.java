/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.CalendarProfileBranchMappingRepository;
import com.misbd.api.calendar.request.CalendarProfileBranchMappingCreateRequest;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarProfileBranchMappingTest {
    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;
    @Autowired
    private CalendarProfileBranchMappingRepository calendarProfileBranchMappingRepository;
    public CalendarProfileBranchMappingTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

     @Test
    public void createCalendarProfileBranchMapping_CalendarProfileId1BranchList_Equal() {
         List<Long> branchList = new ArrayList<>();
        branchList.add(1001L);
        branchList.add(1009L);
        calendarTemplateConfigurationService.createCaldendarProfileBranchMapping(new CalendarProfileBranchMappingCreateRequest(1L, branchList));
        assertEquals(branchList.size(), calendarProfileBranchMappingRepository.findByCalendarProfile_Id(1L, PageRequest.of(0, 5)).size());
    }

    @Test
    public void deleteCalendarProfileBranchMapping_Profile1Branch1002Id4_Null() {
        calendarTemplateConfigurationService.deleteCalendarProfileBranchMapping(4L);
        assertNull(calendarProfileBranchMappingRepository.findById(4L));
    }

    @Test
    public void findAllCalendarBranchMapping_CalendarProfileId1_Equal() {
        assertEquals(calendarTemplateConfigurationService.findAllCalendarBranchMapping(1L, 0, 5).size(), calendarProfileBranchMappingRepository.findByCalendarProfile_Id(1L, PageRequest.of(0, 5)).size());
    }
}
