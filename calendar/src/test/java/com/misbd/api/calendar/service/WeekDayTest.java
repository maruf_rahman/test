/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.entity.WeekDay;
import com.misbd.api.calendar.repository.WeekDayRepository;
import com.misbd.api.calendar.request.WeekDayCreateRequest;
import com.misbd.api.calendar.response.WeekDayResponse;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class WeekDayTest {
    @Autowired
    private WeekDayRepository weekDayRepository;
    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;
    public WeekDayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

        @Test
    public void fillWeekDay_WeekdaySaveRequest_Equal() {
            WeekDayCreateRequest weekDaySaveRequest = new WeekDayCreateRequest(1L, false, true, true, true, true, true, false);
            WeekDay weekDay = calendarTemplateConfigurationService.fillWeekDay(weekDaySaveRequest);
        assertEquals(weekDay.getCalendarProfile().getId(), weekDaySaveRequest.getCalendarProfileId());
    }

    @Test
    public void createWeekDay_ProfileId1FridaySaturdayOff_NotNull() {
        calendarTemplateConfigurationService.createWeekDay(new WeekDayCreateRequest(1L, false, true, true, true, true, true, false));
        assertNotNull(weekDayRepository.findByCalendarProfile_Id(1L));
    }
    
    /**
     *
     */
    @Test
    public void findWeeDay_ProfileId1_Null(){
        WeekDayResponse weekDayResponse = calendarTemplateConfigurationService.findWeeDay(1L);
        if(weekDayResponse==null){
            assertNull(weekDayResponse);
        }else{
            assertNotNull(weekDayResponse);
        }
    }
}
