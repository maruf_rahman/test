/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.RecurringHolidayRepository;
import com.misbd.api.calendar.request.RecurringHolidayCreateRequest;
import com.misbd.api.common.CalendarUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RecurringHolidayTest {
    @Autowired
    private RecurringHolidayRepository recurringHolidayRepository;
    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;
    public RecurringHolidayTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

        @Test
    public void createRecurringHoliday_CalendarProfileId1Holiday20190221_NotNull() throws ParseException {
            Date holiday = CalendarUtils.pareStringToDateyyyyMMdd("2019-02-21");
        calendarTemplateConfigurationService.createRecurringHoliday(new RecurringHolidayCreateRequest(1L, holiday, "International Mother Language Day"));
        assertNotNull(recurringHolidayRepository.findByCalendarProfile_IdAndDayOfHoliday(1L, holiday));
    }

    @Test
    public void createRecurringHoliday_CalendarProfileId1Holiday20190326_NotNull() throws ParseException {
        Date holiday = CalendarUtils.pareStringToDateyyyyMMdd("2019-03-26");
        calendarTemplateConfigurationService.createRecurringHoliday(new RecurringHolidayCreateRequest(1L, holiday, "Independent Day"));
        assertNotNull(recurringHolidayRepository.findByCalendarProfile_IdAndDayOfHoliday(1L, holiday));
    }
    @Test
    public void deleteRecurringHoliday_Id6_NotNull() throws ParseException {
        calendarTemplateConfigurationService.deleteRecurringHoliday(5L);
        assertNull(recurringHolidayRepository.findById(5L));
    }
      @Test
      public void findAllRecurringHoliday_CalendarProfileId1_Equal(){
          assertEquals(2,calendarTemplateConfigurationService.findAllRecurringHoliday(1L, 0, 5).size());
      }
}
