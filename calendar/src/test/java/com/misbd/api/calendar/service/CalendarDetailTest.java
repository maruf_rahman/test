/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.entity.CalendarDetail;
import com.misbd.api.calendar.repository.CalendarDetailRepository;
import com.misbd.api.calendar.request.CalendarDetailCreateRequest;
import com.misbd.api.calendar.request.CalendarDetailUpdateRequest;
import com.misbd.api.common.CalendarUtils;
import com.misbd.api.common.Message;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarDetailTest {

    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;
    @Autowired
    private CalendarDetailRepository calendarDetailRepository;

    @Test
    public void createCalendarDetail_CalendarMasterId120190305And20190310_NotNull() throws ParseException{
        List<CalendarDetailCreateRequest> calendarDetailsCreateRequests = new ArrayList<>();
        calendarDetailsCreateRequests.add(new CalendarDetailCreateRequest(1L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-05"), "description 1"));
        calendarDetailsCreateRequests.add(new CalendarDetailCreateRequest(1L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-10"), "description 2"));
        calendarYearConfigurationService.createCalendarDetail(calendarDetailsCreateRequests);
        assertEquals(2, calendarDetailRepository.findByCalendarMaster_Id(1L, PageRequest.of(0, 10)).size());
    } 
    @Test
    public void findCalendarDetailList_CalendarMasterId1_SizeEqual2() {
        assertEquals(2, calendarYearConfigurationService.findCalendarDetailList(1L, 0, 10).size());
    }

    @Test
    public void updateCalendarDetailList_Id1_EqualDateOfHolidayDescription() throws ParseException {
        CalendarDetailUpdateRequest calendarDetailUpdateRequest = new CalendarDetailUpdateRequest(1L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-06"), "description 1 modified");
        calendarYearConfigurationService.updateCalendarDetails(calendarDetailUpdateRequest);
        CalendarDetail calendarDetail = calendarDetailRepository.getOne(1L);
        assertEquals(calendarDetail.getDateOfHoliday(), calendarDetailUpdateRequest.getDateOfHoliday());
        assertEquals(calendarDetail.getDescription(), calendarDetailUpdateRequest.getDescription());
    }

    @Test
    public void deleteCalendarDetail_Id3_EqualSuccessMessage() {
        assertEquals(Message.DELETED_MSG, calendarYearConfigurationService.deleteCalendarDetail(3L));
    }
}
