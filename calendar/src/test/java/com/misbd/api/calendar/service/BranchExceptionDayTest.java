/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.BranchExceptionDayRepository;
import com.misbd.api.calendar.request.BranchExceptionDayCreateRequest;
import com.misbd.api.common.CalendarUtils;
import com.misbd.api.common.Message;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BranchExceptionDayTest {

    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;
    @Autowired
    private BranchExceptionDayRepository branchExceptionDayRepository;

    public BranchExceptionDayTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createBranchExceptionDay_BranchId1001Date20190301_Equal() throws ParseException {
        List<BranchExceptionDayCreateRequest> branchExceptionDayCreateRequests = new ArrayList<>();
        branchExceptionDayCreateRequests.add(new BranchExceptionDayCreateRequest(1001L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-01"), "Test", false));
        branchExceptionDayCreateRequests.add(new BranchExceptionDayCreateRequest(1001L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-03"), "Test 2", true));
        String message = calendarYearConfigurationService.createBranchExceptionDay(branchExceptionDayCreateRequests);
        assertEquals(message, Message.SAVE_MSG);
    }

    @Test
    public void findBranchExceptionDayList_BranchId1001_Equal() {
        assertEquals(2, calendarYearConfigurationService.findBranchExceptionDayList(1001L, 0, 5).size());
    }

    @Test
    public void findBranchExceptionDayList_ExceptionDay20190301_NotNull() throws ParseException {
        assertEquals(1, calendarYearConfigurationService.findBranchExceptionDayList(CalendarUtils.pareStringToDateyyyyMMdd("2019-03-01"), 0, 5).size());
    }

//    @Test
//    public void findBranchExceptionDayList_BranchId1001ExceptionDay20190301_NotNull() throws ParseException {
//        assertEquals(1, calendarYearConfigurationService.findBranchExceptionDayList(1001L, CalendarUtils.pareStringToDateyyyyMMdd("2019-03-01"), 0, 5).size());
//    }

    @Test
    public void deleteBranchExcetionDay_ID2_Null() {
        calendarYearConfigurationService.deleteBranchExceptionDay(2L);
        assertNull(branchExceptionDayRepository.getOne(2L));
    }
}
