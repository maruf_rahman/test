/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.CalendarProfileBranchMappingRepository;
import com.misbd.api.calendar.repository.CalendarProfileRepository;
import com.misbd.api.calendar.repository.RecurringHolidayRepository;
import com.misbd.api.calendar.repository.WeekDayRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarTemplateConfigurationServiceTest {

    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;
    @Autowired
    private CalendarProfileRepository calendarProfileRepository;
    @Autowired
    private CalendarProfileBranchMappingRepository calendarProfileBranchMappingRepository;
    @Autowired
    private RecurringHolidayRepository recurringHolidayRepository;
    @Autowired
    private WeekDayRepository weekDayRepository;

    public CalendarTemplateConfigurationServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

 
//    @Test
//    public void updateCalendarProfileBranchMapping(){
//        List<Long> branchList = new ArrayList<>();
//        branchList.add(1003L);
//        branchList.add(1004L);
//        calendarTemplateConfigurationService.updateCaldendarProfileBranchMapping(1L,branchList);
//        assertEquals(branchList.size(),calendarProfileBranchMappingRepository.findByCalendarProfile_IdAndBranchIdIn(1L, branchList).size());
//    }
//    

//    @Test
//    public void updateCalendarProfileBranchMapping2(){
//        List<Long> branchList = new ArrayList<>();
//        branchList.add(1001L);
//        branchList.add(1002L);
//        calendarTemplateConfigurationService.updateCaldendarProfileBranchMapping(2L,branchList);
//        assertEquals(branchList.size(),calendarProfileBranchMappingRepository.findByCalendarProfile_IdAndBranchIdIn(2L, branchList).size());
//    }
//    
//    @Test
//    public void deleteCalendarProfileBranchMapping(){
//        List<Long> branchList = new ArrayList<>();
//        branchList.add(1001L);
//        branchList.add(1002L);
//        calendarTemplateConfigurationService.deleteCaldendarProfileBranchMapping()
//    }
}
