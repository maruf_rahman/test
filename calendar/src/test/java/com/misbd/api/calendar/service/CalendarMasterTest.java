/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.repository.CalendarMasterRepository;
import com.misbd.api.calendar.request.CalendarCreateRequest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Maruf
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CalendarMasterTest {

    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;
    @Autowired
    private CalendarMasterRepository calendarMasterRepository;

    @Test
    public void createCalendar_Year2019_NotNull() {
        calendarYearConfigurationService.createCalendar(new CalendarCreateRequest(1L, "2019", "Calendar of 2019"));
        assertNotNull(calendarMasterRepository.findById(1L));
    }

    @Test
    public void findCalendar_All_EqualSize() {
        assertEquals(calendarMasterRepository.findAll().size(), calendarYearConfigurationService.findCalendar(0, 10).size());
    }

    @Test
    public void findCalendar_Year2019_EqualSize() {
        assertEquals(calendarMasterRepository.findByYear("2019", PageRequest.of(0, 10)).size(), calendarYearConfigurationService.findCalendar("2019", 0, 10).size());
    }

    @Test
    public void findCalendar_Profile1_EqualSize1() {
        assertEquals(calendarMasterRepository.findByCalendarProfile_Id(1L).size(), calendarYearConfigurationService.findCalendar(1L).size());
    }

    @Test
    public void findCalendar_Profile1Year2019_NotNUll() {
        assertNotNull(calendarYearConfigurationService.findCalendar(1L));
    }
}
