/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.CalendarMaster;
import java.util.List;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface CalendarMasterRepository extends JpaRepository<CalendarMaster, Long>{

    public List<CalendarMaster> findByYear(String year, Pageable pageable);
    
    public List<CalendarMaster> findByCalendarProfile_Id(Long profileId);

    public CalendarMaster findByYearAndCalendarProfile_Id(String year, Long profileId);
    
    
}
