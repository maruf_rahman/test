/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.List;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class CalendarProfileBranchMappingCreateRequest {
    @NotNull
    private Long profileId;
    @NotNull
    private List<Long> branchList;

    public CalendarProfileBranchMappingCreateRequest() {
    }

    public CalendarProfileBranchMappingCreateRequest(Long profileId, List<Long> branchList) {
        this.profileId = profileId;
        this.branchList = branchList;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public List<Long> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<Long> branchList) {
        this.branchList = branchList;
    }
    
}
