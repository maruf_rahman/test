/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

/**
 *
 * @author ProBook
 */
public class CalendarMasterResponse {
    private Long id;
    private String year;
    private String description;
    private Long profileId;
    private String profileName;

    public CalendarMasterResponse(Long id, String year, String description, Long profileId, String profileName) {
        this.id = id;
        this.year = year;
        this.description = description;
        this.profileId = profileId;
        this.profileName = profileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }
    
}
