/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import com.misbd.api.calendar.request.BranchExceptionDayCreateRequest;
import java.util.Date;

/**
 *
 * @author Maruf
 */
public class BranchExceptionDayResponse extends BranchExceptionDayCreateRequest{
    private Long id;

    public BranchExceptionDayResponse(Long id, Long branchId, Date dayOfException, String description, boolean holiday) {
        super(branchId, dayOfException, description, holiday);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
