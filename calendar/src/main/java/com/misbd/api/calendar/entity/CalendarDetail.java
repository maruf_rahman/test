/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.Proxy;

/**
 *
 * @author Maruf
 */
@Entity
@Proxy(lazy = false)
@Table(name="calendar_detail", uniqueConstraints = @UniqueConstraint(columnNames = {"calendar_master", "date_of_holiday"}))
public class CalendarDetail implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="calendar_master")
    private CalendarMaster calendarMaster;
    
    @Temporal(TemporalType.DATE)
    @Column(name="date_of_holiday")
    private Date dateOfHoliday;
    
    @Column(name="description")
    private String description;

    public CalendarDetail() {
    }

    public CalendarDetail(CalendarMaster calendarMaster, Date dateOfHoliday, String description) {
        this.calendarMaster = calendarMaster;
        this.dateOfHoliday = dateOfHoliday;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarMaster getCalendarMaster() {
        return calendarMaster;
    }

    public void setCalendarMaster(CalendarMaster calendarMaster) {
        this.calendarMaster = calendarMaster;
    }

    public Date getDateOfHoliday() {
        return dateOfHoliday;
    }

    public void setDateOfHoliday(Date dateOfHoliday) {
        this.dateOfHoliday = dateOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
