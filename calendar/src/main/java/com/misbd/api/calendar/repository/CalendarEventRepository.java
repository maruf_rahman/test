/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.CalendarEvent;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface CalendarEventRepository extends JpaRepository<CalendarEvent, Long>{
    public List<CalendarEvent> findByCalendarMaster_Id(Long masterId, Pageable pageable);
}
