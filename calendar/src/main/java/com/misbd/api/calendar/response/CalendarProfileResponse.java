/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

/**
 *
 * @author Maruf
 */
public class CalendarProfileResponse {
    private Long id;
    private String profileName;
    private String description;

    public CalendarProfileResponse() {
    }

    public CalendarProfileResponse(Long id, String profileName, String description) {
        this.id = id;
        this.profileName = profileName;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
