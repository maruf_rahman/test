/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name = "calendar_profile")
public class CalendarProfile implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "calendarProfile", cascade = CascadeType.ALL)
    private List<CalendarProfileBranchMapping> calendarProfileBranchMappings;

    @OneToMany(mappedBy = "calendarProfile", cascade = CascadeType.ALL)
    private List<RecurringHoliday> recurringHolidays;

    @OneToOne(mappedBy = "calendarProfile", cascade = CascadeType.ALL)
    private WeekDay weekDay;

    public CalendarProfile(Long id) {
        this.id = id;
    }
 
    public CalendarProfile(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public CalendarProfile() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CalendarProfileBranchMapping> getCalendarProfileBranchMappings() {
        return calendarProfileBranchMappings;
    }

    public void setCalendarProfileBranchMappings(List<CalendarProfileBranchMapping> calendarProfileBranchMappings) {
        this.calendarProfileBranchMappings = calendarProfileBranchMappings;
    }

    public List<RecurringHoliday> getRecurringHolidays() {
        return recurringHolidays;
    }

    public void setRecurringHolidays(List<RecurringHoliday> recurringHolidays) {
        this.recurringHolidays = recurringHolidays;
    }

    public WeekDay getWeekDay() {
        return weekDay;
    }

    public void setWeekDay(WeekDay weekDay) {
        this.weekDay = weekDay;
    }

}
