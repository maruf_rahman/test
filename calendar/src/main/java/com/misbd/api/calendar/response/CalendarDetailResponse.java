/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import com.misbd.api.calendar.request.CalendarDetailUpdateRequest;
import java.util.Date;

/**
 *
 * @author ProBook
 */
public class CalendarDetailResponse extends CalendarDetailUpdateRequest{
    private Long calendarId;

    public CalendarDetailResponse(Long id, Long calendarId, Date dateOfHoliday, String description) {
        super(id, dateOfHoliday, description);
        this.calendarId = calendarId;
    }

    public Long getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Long calendarId) {
        this.calendarId = calendarId;
    }

    
    
}
