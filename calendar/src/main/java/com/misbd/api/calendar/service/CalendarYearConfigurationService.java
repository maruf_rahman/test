/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.entity.BranchExceptionDay;
import com.misbd.api.calendar.entity.CalendarDetail;
import com.misbd.api.calendar.entity.CalendarEvent;
import com.misbd.api.calendar.entity.CalendarEventType;
import com.misbd.api.calendar.entity.CalendarMaster;
import com.misbd.api.calendar.entity.CalendarProfile;
import com.misbd.api.calendar.entity.RecurringHoliday;
import com.misbd.api.calendar.entity.WeekDay;
import com.misbd.api.calendar.repository.BranchExceptionDayRepository;
import com.misbd.api.calendar.repository.CalendarDetailRepository;
import com.misbd.api.calendar.repository.CalendarEventRepository;
import com.misbd.api.calendar.repository.CalendarEventTypeRepository;
import com.misbd.api.calendar.repository.CalendarMasterRepository;
import com.misbd.api.calendar.repository.CalendarProfileRepository;
import com.misbd.api.calendar.repository.RecurringHolidayRepository;
import com.misbd.api.calendar.repository.WeekDayRepository;
import com.misbd.api.calendar.request.BranchExceptionDayCreateRequest;
import com.misbd.api.calendar.request.CalendarCreateRequest;
import com.misbd.api.calendar.request.CalendarDetailCreateRequest;
import com.misbd.api.calendar.request.CalendarDetailUpdateRequest;
import com.misbd.api.calendar.request.CalendarEventCreateRequest;
import com.misbd.api.calendar.request.CalendarEventTypeCreateRequest;
import com.misbd.api.calendar.request.CalendarEventTypeUpdateRequest;
import com.misbd.api.calendar.request.GenerateCalendarRequest;
import com.misbd.api.calendar.response.BranchExceptionDayResponse;
import com.misbd.api.calendar.response.CalendarDetailResponse;
import com.misbd.api.calendar.response.CalendarEventResponse;
import com.misbd.api.calendar.response.CalendarEventTypeResponse;
import com.misbd.api.calendar.response.CalendarMasterResponse;
import com.misbd.api.calendar.response.CalendarMonthResponse;
import com.misbd.api.calendar.response.CommonResponse;
import com.misbd.api.common.CalendarUtils;
import com.misbd.api.common.Message;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

/**
 *
 * @author Maruf
 */
@Service
public class CalendarYearConfigurationService {

    @Autowired
    private BranchExceptionDayRepository branchExceptionDayRepository;
    @Autowired
    private CalendarEventTypeRepository calendarEventTypeRepository;
    @Autowired
    private CalendarMasterRepository calendarMasterRepository;
    @Autowired
    private CalendarEventRepository calendarEventRepository;
    @Autowired
    private CalendarDetailRepository calendarDetailRepository;
    @Autowired
    private RecurringHolidayRepository recurringHolidayRepository;
    @Autowired
    private WeekDayRepository weekDayRepository;
    @Autowired
    private GenerateCalendarService generateCalendarService;

    public String createBranchExceptionDay(List<BranchExceptionDayCreateRequest> branchExceptionDayCreateRequests) {
        List<BranchExceptionDay> branchExceptionDays = branchExceptionDayCreateRequests.stream().map(obj
                -> new BranchExceptionDay(obj.getBranchId(), obj.getDayOfException(), obj.getDescription(), obj.isHoliday())).collect(Collectors.toList());
        branchExceptionDayRepository.saveAll(branchExceptionDays);
        return Message.SAVE_MSG;
    }

    public BranchExceptionDayResponse provideBranchExceptionRespronse(BranchExceptionDay branchExceptionDay) {
        return new BranchExceptionDayResponse(branchExceptionDay.getId(), branchExceptionDay.getBranchId(), branchExceptionDay.getDateOfException(), branchExceptionDay.getDescription(), branchExceptionDay.isHoliday());
    }

    public List<BranchExceptionDayResponse> findBranchExceptionDayList(Long branchId, int offset, int limit) {
        return branchExceptionDayRepository.findByBranchId(branchId, PageRequest.of(offset, limit)).stream().map(obj -> provideBranchExceptionRespronse(obj)).collect(Collectors.toList());
    }

    public List<BranchExceptionDayResponse> findBranchExceptionDayList(Date dateOfException, int offset, int limit) {
        return branchExceptionDayRepository.findByDateOfException(dateOfException, PageRequest.of(offset, limit)).stream().map(obj -> provideBranchExceptionRespronse(obj)).collect(Collectors.toList());
    }

    public String deleteBranchExceptionDay(Long id) {
        branchExceptionDayRepository.deleteById(id);
        return Message.DELETED_MSG;
    }

    public String createCalendarEventType(CalendarEventTypeCreateRequest calendarEventTypeCreateRequest) {
        calendarEventTypeRepository.saveAndFlush(new CalendarEventType(calendarEventTypeCreateRequest.getName(), calendarEventTypeCreateRequest.getDescription()));
        return Message.SAVE_MSG;
    }

    public String updateCalendarEventType(CalendarEventTypeUpdateRequest calendarEventTypeUpdateRequest) {
        calendarEventTypeRepository.saveAndFlush(new CalendarEventType(calendarEventTypeUpdateRequest.getId(), calendarEventTypeUpdateRequest.getName(), calendarEventTypeUpdateRequest.getDescription()));
        return Message.UPDATED_MSG;
    }

    public List<CalendarEventTypeResponse> findCalendarEventTypeList(int offset, int limit) {
        return calendarEventTypeRepository.findAll(PageRequest.of(offset, limit)).stream().map(obj -> new CalendarEventTypeResponse(obj.getId(), obj.getName(), obj.getDescription())).collect(Collectors.toList());
    }

    public String deleteCalendarEventType(Long id) {
        calendarEventTypeRepository.deleteById(id);
        return Message.DELETED_MSG;
    }

    public String createCalendar(CalendarCreateRequest calendarCreateRequest) {
        calendarMasterRepository.saveAndFlush(new CalendarMaster(new CalendarProfile(calendarCreateRequest.getCalendarProfileId()), calendarCreateRequest.getYear(), calendarCreateRequest.getDescription()));
        return Message.SAVE_MSG;
    }

    public CalendarMasterResponse provideCalendarMasterResponse(CalendarMaster calendarMaster) {
        return new CalendarMasterResponse(calendarMaster.getId(), calendarMaster.getYear(), calendarMaster.getDescription(), calendarMaster.getCalendarProfile().getId(), calendarMaster.getCalendarProfile().getName());
    }

    public List<CalendarMasterResponse> findCalendar(int offset, int limit) {
        return calendarMasterRepository.findAll(PageRequest.of(offset, limit)).stream().map(obj -> provideCalendarMasterResponse(obj)).collect(Collectors.toList());
    }

    public List<CalendarMasterResponse> findCalendar(String year, int offset, int limit) {
        return calendarMasterRepository.findByYear(year, PageRequest.of(offset, limit)).stream().map(obj -> provideCalendarMasterResponse(obj)).collect(Collectors.toList());
    }

    public List<CalendarMasterResponse> findCalendar(Long calendarProfileId) {
        return calendarMasterRepository.findByCalendarProfile_Id(calendarProfileId).stream().map(obj -> provideCalendarMasterResponse(obj)).collect(Collectors.toList());
    }

    public CalendarMasterResponse findCalendarSingle(Long id) {
        return provideCalendarMasterResponse(calendarMasterRepository.getOne(id));
    }

    public String isEventPossible(Long calendarId, List<Long> branchIds, Date eventDate, Long profileId) {
        WeekDay weekDay = weekDayRepository.findByCalendarProfile_Id(profileId);
        if (CalendarUtils.isWeeklyHoliday(weekDay, CalendarUtils.getDayName(eventDate))) {
            return "Weekly holiday";
        }
        if (branchExceptionDayRepository.findByBranchIdInAndDateOfException(branchIds, eventDate) != null) {
            return "Date of Exception";
        }
        if (calendarDetailRepository.findByCalendarMaster_IdAndDateOfHoliday(calendarId, eventDate) != null) {
            return "Holiday";
        }
        if (recurringHolidayRepository.findByCalendarProfile_IdAndDayOfHoliday(profileId, eventDate) != null) {
            return "Holiday";
        }
        return "";
    }

    public String createCalendarEvent(CalendarEventCreateRequest calendarEventCreateRequest) {
        CalendarMaster calendarMaster = calendarMasterRepository.getOne(calendarEventCreateRequest.getCalendarId());
        List<Long> branchIds = calendarMaster.getCalendarProfile().getCalendarProfileBranchMappings().stream().map(obj -> obj.getBranchId()).collect(Collectors.toList());
        String result = isEventPossible(calendarMaster.getId(), branchIds, calendarEventCreateRequest.getDateOfEvent(), calendarMaster.getCalendarProfile().getId());
        if ("".equals(result)) {
            calendarEventRepository.saveAndFlush(new CalendarEvent(new CalendarMaster(
                    calendarEventCreateRequest.getCalendarId()), calendarEventCreateRequest.getDateOfEvent(), new CalendarEventType(calendarEventCreateRequest.getEventTypeId())));
            return Message.SAVE_MSG;
        } else {
            return result;
        }
    }

    public List<CalendarEventResponse> findCalendarEventList(Long calendarId, int offset, int limit) {
        return calendarEventRepository.findByCalendarMaster_Id(calendarId, PageRequest.of(offset, limit)).stream().map(obj
                -> new CalendarEventResponse(obj.getId(), obj.getDateOfEvent(), obj.getCalendarEventType().getId(), obj.getCalendarEventType().getName(), obj.getCalendarEventType().getDescription())
        ).collect(Collectors.toList());
    }

    public String deleteCalendarEvent(Long id) {
        calendarEventRepository.deleteById(id);
        return Message.DELETED_MSG;
    }

    public CalendarDetail provideCalendarDetail(CalendarDetailCreateRequest calendarDetailCreateRequest) {
        return new CalendarDetail(new CalendarMaster(calendarDetailCreateRequest.getCalendarMasterId()),
                calendarDetailCreateRequest.getDateOfHoliday(), calendarDetailCreateRequest.getDescription());
    }

    public String createCalendarDetail(List<CalendarDetailCreateRequest> calendarDetailsCreateRequests) {
        calendarDetailRepository.saveAll(calendarDetailsCreateRequests.stream().map(obj -> provideCalendarDetail(obj)).collect(Collectors.toList()));
        return Message.SAVE_MSG;
    }

    public CalendarDetailResponse provideCalendarDetailResponse(CalendarDetail calendarDetail) {
        return new CalendarDetailResponse(calendarDetail.getId(), calendarDetail.getCalendarMaster().getId(), calendarDetail.getDateOfHoliday(), calendarDetail.getDescription());
    }

    public List<CalendarDetailResponse> findCalendarDetailList(Long calendarId, int offset, int limit) {
        return calendarDetailRepository.findByCalendarMaster_Id(calendarId, PageRequest.of(offset, limit)).stream().map(obj -> provideCalendarDetailResponse(obj)).collect(Collectors.toList());
    }

    public String updateCalendarDetails(CalendarDetailUpdateRequest calendarDetailUpdateRequest) {
        CalendarDetail calendarDetail = calendarDetailRepository.getOne(calendarDetailUpdateRequest.getId());
        calendarDetail.setDateOfHoliday(calendarDetailUpdateRequest.getDateOfHoliday());
        calendarDetail.setDescription(calendarDetailUpdateRequest.getDescription());
        calendarDetailRepository.saveAndFlush(calendarDetail);
        return Message.UPDATED_MSG;
    }

    public String deleteCalendarDetail(Long id) {
        calendarDetailRepository.deleteById(id);
        return Message.DELETED_MSG;
    }
    
    public List<CalendarMonthResponse> generateCalendar(Long profileId, Long branchId, String year) {
        List<CalendarMonthResponse> calendarMonthResponses = new ArrayList<>();

        CalendarMaster calendarMaster = calendarMasterRepository.findByYearAndCalendarProfile_Id(year, profileId);
        List<BranchExceptionDay> branchExceptionDays = branchExceptionDayRepository.findByBranchId(branchId);
        try {
            if (calendarMaster != null) {
                generateCalendarService.generateDays(calendarMonthResponses, calendarMaster, branchExceptionDays, branchId, year);
            }
        } catch (ParseException ex) {
            Logger.getLogger(GenerateCalendarService.class.getName()).log(Level.SEVERE, null, ex);
        }

        return calendarMonthResponses;
    }
}
