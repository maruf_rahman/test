/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name="calendar_master", uniqueConstraints = @UniqueConstraint(columnNames = {"calendar_profile","year"}))
public class CalendarMaster implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @OneToOne
    @JoinColumn(name="calendar_profile")
    private CalendarProfile calendarProfile;
    
    @Column(name="year")
    private String year;
    
    @Column(name="description")
    private String description;
    
    @OneToMany(mappedBy = "calendarMaster", cascade = CascadeType.ALL)
    private List<CalendarDetail> calendarDetails;
    
    @OneToMany(mappedBy = "calendarMaster", cascade = CascadeType.ALL)
    private List<CalendarEvent> calendarEvents;

    public CalendarMaster() {
    }

    public CalendarMaster(Long id) {
        this.id = id;
    }

    public CalendarMaster(CalendarProfile calendarProfile, String year, String description) {
        this.calendarProfile = calendarProfile;
        this.year = year;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarProfile getCalendarProfile() {
        return calendarProfile;
    }

    public void setCalendarProfile(CalendarProfile calendarProfile) {
        this.calendarProfile = calendarProfile;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CalendarDetail> getCalendarDetails() {
        return calendarDetails;
    }

    public void setCalendarDetails(List<CalendarDetail> calendarDetails) {
        this.calendarDetails = calendarDetails;
    }

    public List<CalendarEvent> getCalendarEvents() {
        return calendarEvents;
    }

    public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
        this.calendarEvents = calendarEvents;
    }
    
    
}
