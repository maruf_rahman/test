/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.RecurringHoliday;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author ProBook
 */
public interface RecurringHolidayRepository extends JpaRepository<RecurringHoliday, Long>{
    public List<RecurringHoliday> findByCalendarProfile_Id(Long profileId, Pageable pageable); 
    public RecurringHoliday findByCalendarProfile_IdAndDayOfHoliday(Long profileId, Date dayOfHoliday); 

    public List<RecurringHoliday> findByDayOfHoliday(Date date);
}
