/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class WeekDayCreateRequest {
    @NotNull
    private Long calendarProfileId;
    @NotNull
    private boolean saturday;
    @NotNull
    private boolean sunday;
    @NotNull
    private boolean monday;
    @NotNull
    private boolean tuesday;
    @NotNull
    private boolean wednesday;
    @NotNull
    private boolean thursday;
    @NotNull
    private boolean friday;

    public WeekDayCreateRequest() {
    }

    public WeekDayCreateRequest(Long calendarProfileId, boolean saturday, boolean sunday, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday) {
        this.calendarProfileId = calendarProfileId;
        this.saturday = saturday;
        this.sunday = sunday;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }

    public Long getCalendarProfileId() {
        return calendarProfileId;
    }

    public void setCalendarProfileId(Long calendarProfileId) {
        this.calendarProfileId = calendarProfileId;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }
    
}
