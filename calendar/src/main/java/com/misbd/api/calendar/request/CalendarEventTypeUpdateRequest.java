/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

/**
 *
 * @author Maruf
 */
public class CalendarEventTypeUpdateRequest extends CalendarEventTypeCreateRequest{
    private Long id;

    public CalendarEventTypeUpdateRequest() {
    }
    
    public CalendarEventTypeUpdateRequest(Long id, String name, String description) {
        super(name, description);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
