/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.CalendarProfileBranchMapping;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface CalendarProfileBranchMappingRepository extends JpaRepository<CalendarProfileBranchMapping, Long>{
    public List<CalendarProfileBranchMapping> findByCalendarProfile_Id(Long profileId, Pageable pageable);
    public List<CalendarProfileBranchMapping> findByCalendarProfile_IdAndBranchIdIn(Long profileId, List<Long> branchList); 
}
