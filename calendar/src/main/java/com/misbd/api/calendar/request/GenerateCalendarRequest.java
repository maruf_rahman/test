/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author ProBook
 */
public class GenerateCalendarRequest {
    @NotNull
    private Long profileId;
    
    @NotNull
    private Long branchId;
    
    @NotNull
    private String year;

    public GenerateCalendarRequest() {
    }

    public GenerateCalendarRequest(Long profileId, Long branchId, String year) {
        this.profileId = profileId;
        this.branchId = branchId;
        this.year = year;
    }

    public Long getProfileId() {
        return profileId;
    }

    public void setProfileId(Long profileId) {
        this.profileId = profileId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }
    
    
}
