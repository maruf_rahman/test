/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.calendar.entity.CalendarProfileBranchMapping;
import com.misbd.api.calendar.entity.CalendarProfile;
import com.misbd.api.calendar.entity.RecurringHoliday;
import com.misbd.api.calendar.entity.WeekDay;
import com.misbd.api.calendar.repository.CalendarProfileBranchMappingRepository;
import com.misbd.api.calendar.repository.CalendarProfileRepository;
import com.misbd.api.calendar.repository.RecurringHolidayRepository;
import com.misbd.api.calendar.repository.WeekDayRepository;
import com.misbd.api.calendar.request.CalendarProfileBranchMappingCreateRequest;
import com.misbd.api.calendar.request.CalendarProfileCreateRequest;
import com.misbd.api.calendar.request.RecurringHolidayCreateRequest;
import com.misbd.api.calendar.request.WeekDayCreateRequest;
import com.misbd.api.calendar.response.CalendarProfileResponse;
import com.misbd.api.calendar.response.CommonResponse;
import com.misbd.api.calendar.response.RecurringHolidayResponse;
import com.misbd.api.calendar.response.WeekDayResponse;
import com.misbd.api.common.Message;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Maruf
 */
@Service
public class CalendarTemplateConfigurationService {

    @Autowired
    private CalendarProfileRepository calendarProfileRepository;
    @Autowired
    private CalendarProfileBranchMappingRepository calendarProfileBranchMappingRepository;
    @Autowired
    private RecurringHolidayRepository recurringHolidayRepository;
    @Autowired
    private WeekDayRepository weekDayRepository;

    public String createCaldendarProfile(CalendarProfileCreateRequest calendarProfileSaveRequest) {
        calendarProfileRepository.save(new CalendarProfile(calendarProfileSaveRequest.getName(), calendarProfileSaveRequest.getDescription()));
        return Message.SAVE_MSG;
    }

    public List<CalendarProfileResponse> findCalendarProfileList(int offset, int limit) {
        return calendarProfileRepository.findAll(PageRequest.of(offset, limit)).stream().map(calendarProfile ->
                new CalendarProfileResponse(calendarProfile.getId(), calendarProfile.getName(), calendarProfile.getDescription())).collect(Collectors.toList());
    }
    
    public List<CalendarProfileBranchMapping> provideCalendarProfileBranchMappingEntity(Long profileId, List<Long> branchList) {
        List<CalendarProfileBranchMapping> calendarProfileBranchMappingEntitys = new ArrayList<>();
        branchList.stream().forEach((branchId) -> {
            calendarProfileBranchMappingEntitys.add(new CalendarProfileBranchMapping(new CalendarProfile(profileId), branchId));
        });
        return calendarProfileBranchMappingEntitys;
    }

    @Transactional
    public String createCaldendarProfileBranchMapping(CalendarProfileBranchMappingCreateRequest calendarProfileBranchMappingSaveRequest) {
//        List<CalendarProfileBranchMapping> calendarProfileBranchMappingEntitys = calendarProfileBranchMappingRepository.findByCalendarProfile_Id(calendarProfileBranchMappingSaveRequest.getProfileId());
//        if (calendarProfileBranchMappingEntitys != null) {
//            calendarProfileBranchMappingRepository.deleteAll(calendarProfileBranchMappingEntitys);
//            calendarProfileBranchMappingRepository.flush();
//        }
        calendarProfileBranchMappingRepository.saveAll(provideCalendarProfileBranchMappingEntity(calendarProfileBranchMappingSaveRequest.getProfileId(), calendarProfileBranchMappingSaveRequest.getBranchList()));
        return Message.SAVE_MSG;
    }

    public String deleteCalendarProfileBranchMapping(Long id) {
        calendarProfileBranchMappingRepository.deleteById(id);
        return Message.DELETED_MSG;
    }
    
    public List<CommonResponse> provideBrunchList(List<CalendarProfileBranchMapping> calendarProfileBranchMappingEntitys) {
        return calendarProfileBranchMappingEntitys.stream()
                .map(calendarProfileBranchMappingEntity -> new CommonResponse(calendarProfileBranchMappingEntity.getBranchId(), calendarProfileBranchMappingEntity.getBranchId()))
                .collect(Collectors.toList());
    }

    public List<CommonResponse> findAllCalendarBranchMapping(Long profileId, int offset, int limit) {
        return provideBrunchList(calendarProfileBranchMappingRepository.findByCalendarProfile_Id(profileId, PageRequest.of(offset, limit)));
    }

    public String createRecurringHoliday(RecurringHolidayCreateRequest recurringHolidaySaveRequest) {
        if (recurringHolidayRepository.findByCalendarProfile_IdAndDayOfHoliday(1L, recurringHolidaySaveRequest.getDateOfHoliday()) == null) {
            recurringHolidayRepository.save(new RecurringHoliday(new CalendarProfile(recurringHolidaySaveRequest.getCaledarProfileId()), recurringHolidaySaveRequest.getDateOfHoliday(), recurringHolidaySaveRequest.getDescription()));
        }
        return Message.SAVE_MSG;
    }

    public String deleteRecurringHoliday(Long id) {
        recurringHolidayRepository.deleteById(id);
        return Message.DELETED_MSG;
    }

    public List<RecurringHolidayResponse> findAllRecurringHoliday(long profileId, int offset, int limit) {
        return recurringHolidayRepository.findByCalendarProfile_Id(profileId, PageRequest.of(offset, limit)).stream()
                .map(recurringHolidayEntity -> new RecurringHolidayResponse(recurringHolidayEntity.getId(), recurringHolidayEntity.getDayOfHoliday(), recurringHolidayEntity.getDescription()))
                .collect(Collectors.toList());
    }

    public WeekDay fillWeekDay(WeekDayCreateRequest weekDaySaveRequest) {
        return new WeekDay(new CalendarProfile(weekDaySaveRequest.getCalendarProfileId()), weekDaySaveRequest.isSaturday(), weekDaySaveRequest.isSunday(),
                weekDaySaveRequest.isMonday(), weekDaySaveRequest.isTuesday(), weekDaySaveRequest.isWednesday(), weekDaySaveRequest.isThursday(), weekDaySaveRequest.isFriday());
    }

    public String createWeekDay(WeekDayCreateRequest weekDaySaveRequest) {
        WeekDay weekDay = weekDayRepository.findByCalendarProfile_Id(weekDaySaveRequest.getCalendarProfileId());
        Long id = null;
        if (weekDay != null) {
            id = weekDay.getId();
        }
        weekDay = fillWeekDay(weekDaySaveRequest);
        weekDay.setId(id);
        weekDayRepository.save(weekDay);
        return Message.DELETED_MSG;
    }

    public WeekDayResponse findWeeDay(long profileId) {
        WeekDay weekDay= weekDayRepository.findByCalendarProfile_Id(profileId);
        if(weekDay!=null){
        return new WeekDayResponse(weekDay.getId(), weekDay.getCalendarProfile().getId(), weekDay.isSaturday(),
                weekDay.isSunday(), weekDay.isMonday(), weekDay.isTuesday(), weekDay.isWednesday(),
                weekDay.isThursday(), weekDay.isFriday());
        }else{
            return null;
        }
        
    }
}
