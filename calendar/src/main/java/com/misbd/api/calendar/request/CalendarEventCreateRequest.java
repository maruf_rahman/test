/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class CalendarEventCreateRequest {
    @NotNull
    private Long calendarId;
    @NotNull
    private Long eventTypeId;
    @NotNull
    private Date dateOfEvent;

    public CalendarEventCreateRequest() {
    }

    public CalendarEventCreateRequest(Long calendarId, Long eventTypeId, Date dateOfEvent) {
        this.calendarId = calendarId;
        this.eventTypeId = eventTypeId;
        this.dateOfEvent = dateOfEvent;
    }

    public Long getCalendarId() {
        return calendarId;
    }

    public void setCalendarId(Long calendarId) {
        this.calendarId = calendarId;
    }

    public Long getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }
    
}
