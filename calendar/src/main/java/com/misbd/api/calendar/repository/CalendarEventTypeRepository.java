/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.CalendarEventType;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface CalendarEventTypeRepository extends JpaRepository<CalendarEventType, Long>{

    public CalendarEventType findByName(String string);
    
}
