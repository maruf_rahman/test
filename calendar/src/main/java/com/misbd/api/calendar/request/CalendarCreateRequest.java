/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

/**
 *
 * @author Maruf
 */
public class CalendarCreateRequest {
    private Long calendarProfileId;
    private String year;
    private String description;

    public CalendarCreateRequest() {
    }

    public CalendarCreateRequest(Long calendarProfileId, String year, String description) {
        this.calendarProfileId = calendarProfileId;
        this.year = year;
        this.description = description;
    }

    public Long getCalendarProfileId() {
        return calendarProfileId;
    }

    public void setCalendarProfileId(Long calendarProfileId) {
        this.calendarProfileId = calendarProfileId;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
