/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class BranchExceptionDayCreateRequest {
    @NotNull
    private Long branchId;
    @NotNull
    private Date dayOfException;
    private String description;
    @NotNull
    private boolean holiday;

    public BranchExceptionDayCreateRequest() {
    }

    public BranchExceptionDayCreateRequest(Long branchId, Date dayOfException, String description, boolean holiday) {
        this.branchId = branchId;
        this.dayOfException = dayOfException;
        this.description = description;
        this.holiday = holiday;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Date getDayOfException() {
        return dayOfException;
    }

    public void setDayOfException(Date dayOfException) {
        this.dayOfException = dayOfException;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }
    
}
