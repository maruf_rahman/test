/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ProBook
 */
public class CalendarDetailUpdateRequest{
    @NotNull
    private Long id;
    @NotNull
    private Date dateOfHoliday;
    private String description;

    public CalendarDetailUpdateRequest() {
    }

    public CalendarDetailUpdateRequest(Long id, Date dateOfHoliday, String description) {
        this.id = id;
        this.dateOfHoliday = dateOfHoliday;
        this.description = description;
    }
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateOfHoliday() {
        return dateOfHoliday;
    }

    public void setDateOfHoliday(Date dateOfHoliday) {
        this.dateOfHoliday = dateOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
    
}
