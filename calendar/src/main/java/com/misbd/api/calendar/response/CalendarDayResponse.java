/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import java.util.Date;

/**
 *
 * @author ProBook
 */
public class CalendarDayResponse {
    private String dayName;
    private int day;
    private Date date;
    private String holiday;
    private boolean weeklyHoliday;
    private String exceptionDay;
    private boolean exceptionHoliday;
    private String event;
    private String eventDescription;

    public CalendarDayResponse() {
    }

    public CalendarDayResponse(String dayName, int day, Date date, String holiday, boolean weeklyHoliday, String exceptionDay, boolean exceptionHoliday, String event, String eventDescription) {
        this.dayName = dayName;
        this.day = day;
        this.date = date;
        this.holiday = holiday;
        this.weeklyHoliday = weeklyHoliday;
        this.exceptionDay = exceptionDay;
        this.exceptionHoliday = exceptionHoliday;
        this.event = event;
        this.eventDescription = eventDescription;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getExceptionDay() {
        return exceptionDay;
    }

    public void setExceptionDay(String exceptionDay) {
        this.exceptionDay = exceptionDay;
    }

    public boolean isExceptionHoliday() {
        return exceptionHoliday;
    }

    public void setExceptionHoliday(boolean exceptionHoliday) {
        this.exceptionHoliday = exceptionHoliday;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public boolean isWeeklyHoliday() {
        return weeklyHoliday;
    }

    public void setWeeklyHoliday(boolean weeklyHoliday) {
        this.weeklyHoliday = weeklyHoliday;
    }
    
}
