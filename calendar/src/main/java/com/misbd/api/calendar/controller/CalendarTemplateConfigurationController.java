/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.controller;

import com.misbd.api.calendar.request.CalendarProfileBranchMappingCreateRequest;
import com.misbd.api.calendar.request.CalendarProfileCreateRequest;
import com.misbd.api.calendar.request.RecurringHolidayCreateRequest;
import com.misbd.api.calendar.request.WeekDayCreateRequest;
import com.misbd.api.calendar.response.CalendarProfileResponse;
import com.misbd.api.calendar.response.CommonResponse;
import com.misbd.api.calendar.response.RecurringHolidayResponse;
import com.misbd.api.calendar.response.WeekDayResponse;
import com.misbd.api.calendar.service.CalendarTemplateConfigurationService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Maruf
 */
@RestController
@RequestMapping(value = "/api/v1")
public class CalendarTemplateConfigurationController {

    @Autowired
    private CalendarTemplateConfigurationService calendarTemplateConfigurationService;

    @PostMapping("/calendar-profiles")
    public ResponseEntity<String> createCaldendarProfile(@RequestBody CalendarProfileCreateRequest calendarProfileSaveRequest) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.createCaldendarProfile(calendarProfileSaveRequest), HttpStatus.CREATED);
    }

    @GetMapping("/calendar-profiles")
    public ResponseEntity<List<CalendarProfileResponse>> createCaldendarProfile(@RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.findCalendarProfileList(offset, limit), HttpStatus.OK);
    }

    @PostMapping("/calendar-profiles/branch-mappings")
    public ResponseEntity<String> createCaldendarProfileBranchMapping(@RequestBody CalendarProfileBranchMappingCreateRequest calendarProfileBranchMappingSaveRequest) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.createCaldendarProfileBranchMapping(calendarProfileBranchMappingSaveRequest), HttpStatus.CREATED);
    }

    @DeleteMapping("/calendar-profiles/branch-mappings/{id}")
    public ResponseEntity<String> deleteCalendarProfileBranchMapping(@PathVariable Long id) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.deleteCalendarProfileBranchMapping(id), HttpStatus.OK);
    }

    @GetMapping("/calendar-profiles/{profileId}/branch-mappings")
    public ResponseEntity<List<CommonResponse>> findAllCalendarBranchMapping(@PathVariable Long profileId, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.findAllCalendarBranchMapping(profileId, offset, limit), HttpStatus.OK);
    }

    @PostMapping("/recurring-holidays")
    public ResponseEntity<String> createRecurringHoliday(@RequestBody RecurringHolidayCreateRequest recurringHolidaySaveRequest) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.createRecurringHoliday(recurringHolidaySaveRequest), HttpStatus.CREATED);
    }

    @GetMapping("/recurring-holidays/{profileId}")
    public ResponseEntity<List<RecurringHolidayResponse>> findAllRecurringHoliday(@PathVariable Long profileId, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.findAllRecurringHoliday(profileId, offset, limit), HttpStatus.OK);
    }

    @DeleteMapping("/recurring-holidays/{id}")
    public ResponseEntity<String> deleteRecurringHoliday(@PathVariable Long id) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.deleteRecurringHoliday(id), HttpStatus.OK);
    }

    @PostMapping("/week-days")
    public ResponseEntity<String> createWeekDay(@RequestBody WeekDayCreateRequest weekDaySaveRequest) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.createWeekDay(weekDaySaveRequest), HttpStatus.CREATED);
    }

    @GetMapping("/week-days/{profileId}")
    public ResponseEntity<WeekDayResponse> findWeeDay(@PathVariable Long profileId) {
        return new ResponseEntity<>(calendarTemplateConfigurationService.findWeeDay(profileId), HttpStatus.OK);
    }
}
