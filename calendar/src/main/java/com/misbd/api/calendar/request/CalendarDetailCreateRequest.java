/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ProBook
 */
public class CalendarDetailCreateRequest {
    @NotNull
    private Long calendarMasterId;
    @NotNull
    private Date dateOfHoliday;
    private String description;

    public CalendarDetailCreateRequest() {
    }

    public CalendarDetailCreateRequest(Long calendarMasterId, Date dateOfHoliday, String description) {
        this.calendarMasterId = calendarMasterId;
        this.dateOfHoliday = dateOfHoliday;
        this.description = description;
    }

    public Long getCalendarMasterId() {
        return calendarMasterId;
    }

    public void setCalendarMasterId(Long calendarMasterId) {
        this.calendarMasterId = calendarMasterId;
    }

    public Date getDateOfHoliday() {
        return dateOfHoliday;
    }

    public void setDateOfHoliday(Date dateOfHoliday) {
        this.dateOfHoliday = dateOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
