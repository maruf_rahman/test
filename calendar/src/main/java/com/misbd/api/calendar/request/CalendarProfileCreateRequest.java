/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class CalendarProfileCreateRequest {
    @NotNull
    private String name;
    private String description;

    public CalendarProfileCreateRequest() {
    }

    public CalendarProfileCreateRequest(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
