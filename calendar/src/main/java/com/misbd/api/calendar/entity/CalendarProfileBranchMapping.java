/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name = "calendar_profile_branch_mapping")
public class CalendarProfileBranchMapping implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "calendar_profile")
    private CalendarProfile calendarProfile;
    
    @Column(name="branch_id", nullable = false, unique = true)
    private Long branchId;

    public CalendarProfileBranchMapping() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarProfileBranchMapping(CalendarProfile calendarProfile, Long branchId) {
        this.calendarProfile = calendarProfile;
        this.branchId = branchId;
    }

    public CalendarProfile getCalendarProfile() {
        return calendarProfile;
    }

    public void setCalendarProfile(CalendarProfile calendarProfile) {
        this.calendarProfile = calendarProfile;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }
    
}
