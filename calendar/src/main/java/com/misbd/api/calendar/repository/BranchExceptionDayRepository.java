/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.BranchExceptionDay;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface BranchExceptionDayRepository extends JpaRepository<BranchExceptionDay, Long>{

    public List<BranchExceptionDay> findByBranchId(long branchId, Pageable pageable);

    public List<BranchExceptionDay> findByDateOfException(Date dateOfException, Pageable pageable);
    
    public BranchExceptionDay findByBranchIdInAndDateOfException(List<Long> branchIds, Date dateOfExceptiond);

    public List<BranchExceptionDay> findByBranchId(Long branchId);

    public List<BranchExceptionDay> findByDateOfException(Date date);
    
}
