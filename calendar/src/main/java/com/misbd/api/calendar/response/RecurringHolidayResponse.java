/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import java.util.Date;

/**
 *
 * @author Maruf
 */
public class RecurringHolidayResponse {
    private Long id;
    private Date dayOfHoliday;
    private String description;

    public RecurringHolidayResponse(Long id, Date dayOfHoliday, String description) {
        this.id = id;
        this.dayOfHoliday = dayOfHoliday;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDayOfHoliday() {
        return dayOfHoliday;
    }

    public void setDayOfHoliday(Date dayOfHoliday) {
        this.dayOfHoliday = dayOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
