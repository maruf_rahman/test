/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.WeekDay;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface WeekDayRepository extends JpaRepository<WeekDay, Long>{

    public WeekDay findByCalendarProfile_Id(Long profileId);
    
}
