/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.controller;

import com.misbd.api.calendar.request.BranchExceptionDayCreateRequest;
import com.misbd.api.calendar.request.CalendarCreateRequest;
import com.misbd.api.calendar.request.CalendarDetailCreateRequest;
import com.misbd.api.calendar.request.CalendarDetailUpdateRequest;
import com.misbd.api.calendar.request.CalendarEventCreateRequest;
import com.misbd.api.calendar.request.CalendarEventTypeCreateRequest;
import com.misbd.api.calendar.request.CalendarEventTypeUpdateRequest;
import com.misbd.api.calendar.response.BranchExceptionDayResponse;
import com.misbd.api.calendar.response.CalendarDetailResponse;
import com.misbd.api.calendar.response.CalendarEventResponse;
import com.misbd.api.calendar.response.CalendarEventTypeResponse;
import com.misbd.api.calendar.response.CalendarMasterResponse;
import com.misbd.api.calendar.response.CalendarMonthResponse;
import com.misbd.api.calendar.response.CommonResponse;
import com.misbd.api.calendar.service.CalendarYearConfigurationService;
import com.misbd.api.calendar.service.GenerateCalendarService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Maruf
 */
@RestController
@RequestMapping(value = "api/v1")
public class CalendarYearConfigurationController {

    @Autowired
    private CalendarYearConfigurationService calendarYearConfigurationService;

    @PostMapping("/branch-exception-days")
    public ResponseEntity<String> createBranchExceptionDay(@RequestBody List<BranchExceptionDayCreateRequest> branchExceptionDayCreateRequests) {
        return new ResponseEntity<>(calendarYearConfigurationService.createBranchExceptionDay(branchExceptionDayCreateRequests), HttpStatus.CREATED);
    }

    @GetMapping("/branch-exception-days/{branchId}")
    public ResponseEntity<List<BranchExceptionDayResponse>> createCaldendarProfile(@PathVariable Long branchId, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findBranchExceptionDayList(branchId, offset, limit), HttpStatus.OK);
    }

    @GetMapping("/branch-exception-days")
    public ResponseEntity<List<BranchExceptionDayResponse>> createCaldendarProfile(@RequestParam @DateTimeFormat(pattern = "dd/MM/yyyy") Date dateOfException, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findBranchExceptionDayList(dateOfException, offset, limit), HttpStatus.OK);
    }

    @DeleteMapping("/branch-exception-days/{id}")
    public ResponseEntity<String> deleteBranchExceptionDay(@PathVariable Long id) {
        return new ResponseEntity<>(calendarYearConfigurationService.deleteBranchExceptionDay(id), HttpStatus.OK);
    }

    @PostMapping("/calendar-event-types")
    public ResponseEntity<String> createCalendarEventType(@RequestBody CalendarEventTypeCreateRequest calendarEventTypeCreateRequest) {
        return new ResponseEntity<>(calendarYearConfigurationService.createCalendarEventType(calendarEventTypeCreateRequest), HttpStatus.CREATED);
    }

    @PutMapping("/calendar-event-types")
    public ResponseEntity<String> updateCalendarEventType(@RequestBody CalendarEventTypeUpdateRequest calendarEventTypeUpdateRequest) {
        return new ResponseEntity<>(calendarYearConfigurationService.updateCalendarEventType(calendarEventTypeUpdateRequest), HttpStatus.CREATED);
    }

    @GetMapping("/calendar-event-types")
    public ResponseEntity<List<CalendarEventTypeResponse>> findCalendarEventTypeList(@RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendarEventTypeList(offset, limit), HttpStatus.OK);
    }

    @DeleteMapping("/calendar-event-types/{id}")
    public ResponseEntity<String> deleteCalendarEventType(@PathVariable Long id) {
        return new ResponseEntity<>(calendarYearConfigurationService.deleteCalendarEventType(id), HttpStatus.OK);
    }

    @PostMapping("/calendars")
    public ResponseEntity<String> createCalendar(@RequestBody CalendarCreateRequest calendarCreateRequest) {
        return new ResponseEntity<>(calendarYearConfigurationService.createCalendar(calendarCreateRequest), HttpStatus.CREATED);
    }

    @GetMapping("/calendars")
    public ResponseEntity<List<CalendarMasterResponse>> findCalendar(@RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendar(offset, limit), HttpStatus.CREATED);
    }

    @GetMapping("/calendars/{year}")
    public ResponseEntity<List<CalendarMasterResponse>> findCalendar(@PathVariable String year, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendar(year, offset, limit), HttpStatus.CREATED);
    }

    @GetMapping("/calendars/single/{id}")
    public ResponseEntity<CalendarMasterResponse> findCalendarSingle(@PathVariable Long id) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendarSingle(id), HttpStatus.CREATED);
    }

    @PostMapping("/calendar-events")
    public ResponseEntity<String> createCalendarEvent(@RequestBody CalendarEventCreateRequest calendarEventCreateRequest) {
        return new ResponseEntity<>(calendarYearConfigurationService.createCalendarEvent(calendarEventCreateRequest), HttpStatus.CREATED);
    }

    @GetMapping("/calendar-events/{calendarId}")
    public ResponseEntity<List<CalendarEventResponse>> findCalendarEventList(@PathVariable Long calendarId, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendarEventList(calendarId, offset, limit), HttpStatus.CREATED);
    }

    @DeleteMapping("/calendar-events/{id}")
    public ResponseEntity<String> deleteCalendarEvent(@PathVariable Long id) {
        return new ResponseEntity<>(calendarYearConfigurationService.deleteCalendarEvent(id), HttpStatus.OK);
    }

    @PostMapping("/calendar-details")
    public ResponseEntity<String> createCalendarDetail(@RequestBody List<CalendarDetailCreateRequest> calendarDetailsCreateRequests) {
        return new ResponseEntity<>(calendarYearConfigurationService.createCalendarDetail(calendarDetailsCreateRequests), HttpStatus.CREATED);
    }

    @GetMapping("/calendar-details/{calendarId}")
    public ResponseEntity<List<CalendarDetailResponse>> findCalendarDetailList(@PathVariable Long calendarId, @RequestParam int offset, @RequestParam int limit) {
        return new ResponseEntity<>(calendarYearConfigurationService.findCalendarDetailList(calendarId, offset, limit), HttpStatus.CREATED);
    }

    @PutMapping("/calendar-details")
    public ResponseEntity<String> updateCalendarDetails(@RequestBody CalendarDetailUpdateRequest calendarDetailUpdateRequest) {
        return new ResponseEntity<>(calendarYearConfigurationService.updateCalendarDetails(calendarDetailUpdateRequest), HttpStatus.CREATED);
    }

    @DeleteMapping("/calendar-details/{id}")
    public ResponseEntity<String> deleteCalendarDetail(@PathVariable Long id) {
        return new ResponseEntity<>(calendarYearConfigurationService.deleteCalendarDetail(id), HttpStatus.OK);
    }

    @GetMapping("/calendars/profileId/{profileId}/branchId/{branchId}/year/{year}")
    public ResponseEntity<List<CalendarMonthResponse>> generateCalendar(@PathVariable Long profileId, @PathVariable Long branchId, @PathVariable String year) {
        return new ResponseEntity<>(calendarYearConfigurationService.generateCalendar(profileId, branchId, year), HttpStatus.CREATED);
    }
}
