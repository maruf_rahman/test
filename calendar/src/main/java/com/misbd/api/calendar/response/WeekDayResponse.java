/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import com.misbd.api.calendar.request.WeekDayCreateRequest;

/**
 *
 * @author Maruf
 */
public class WeekDayResponse extends WeekDayCreateRequest{
    private Long id;
    public WeekDayResponse(Long id, Long calendarProfileId, boolean saturday, boolean sunday, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday) {
        super(calendarProfileId, saturday, sunday, monday, tuesday, wednesday, thursday, friday);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
}
