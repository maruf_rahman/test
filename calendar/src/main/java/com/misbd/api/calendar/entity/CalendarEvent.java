/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name="calendar_event")
public class CalendarEvent implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @OneToOne
    @JoinColumn(name="calendar_master")
    private CalendarMaster calendarMaster;
    
    @Temporal(TemporalType.DATE)
    @Column(name="date_of_event")
    private Date dateOfEvent;
    
    @OneToOne
    @JoinColumn(name="calendar_event_type")
    private CalendarEventType calendarEventType;

    public CalendarEvent() {
    }

    public CalendarEvent(CalendarMaster calendarMaster, Date dateOfEvent, CalendarEventType calendarEventType) {
        this.calendarMaster = calendarMaster;
        this.dateOfEvent = dateOfEvent;
        this.calendarEventType = calendarEventType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarMaster getCalendarMaster() {
        return calendarMaster;
    }

    public void setCalendarMaster(CalendarMaster calendarMaster) {
        this.calendarMaster = calendarMaster;
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public CalendarEventType getCalendarEventType() {
        return calendarEventType;
    }

    public void setCalendarEventType(CalendarEventType calendarEventType) {
        this.calendarEventType = calendarEventType;
    }
    
    
}
