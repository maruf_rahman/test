/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

/**
 *
 * @author ProBook
 * @param <T>
 */
public class CommonResponse<T> {
    private T label;
    private T value;

    public CommonResponse() {
    }

    public T getLabel() {
        return label;
    }

    public void setLabel(T label) {
        this.label = label;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public CommonResponse(T label, T value) {
        this.label = label;
        this.value = value;
    }
    
}
