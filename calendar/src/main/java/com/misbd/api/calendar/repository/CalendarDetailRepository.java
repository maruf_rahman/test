/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.repository;

import com.misbd.api.calendar.entity.CalendarDetail;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Maruf
 */
public interface CalendarDetailRepository extends JpaRepository<CalendarDetail, Long>{

    public List<CalendarDetail> findByCalendarMaster_Id(long calendarMasterId, Pageable pageable);
    
    public CalendarDetail findByCalendarMaster_IdAndDateOfHoliday(long calendarMasterId, Date dateOfHoliday);

    public List<CalendarDetail> findByDateOfHoliday(Date date);
    
}
