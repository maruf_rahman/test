/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import com.misbd.api.calendar.request.CalendarEventTypeUpdateRequest;

/**
 *
 * @author Maruf
 */
public class CalendarEventTypeResponse extends CalendarEventTypeUpdateRequest{

    public CalendarEventTypeResponse(Long id, String name, String description) {
        super(id, name, description);
    }
    
}
