/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.service;

import com.misbd.api.common.GenerateCalendar;
import com.misbd.api.calendar.entity.BranchExceptionDay;
import com.misbd.api.calendar.entity.CalendarDetail;
import com.misbd.api.calendar.entity.CalendarEvent;
import com.misbd.api.calendar.entity.CalendarMaster;
import com.misbd.api.calendar.entity.RecurringHoliday;
import com.misbd.api.calendar.entity.WeekDay;
import com.misbd.api.calendar.response.CalendarDayResponse;
import com.misbd.api.calendar.response.CalendarMonthResponse;
import com.misbd.api.common.CalendarUtils;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import org.springframework.stereotype.Service;

/**
 *
 * @author Maruf
 */
@Service
public class GenerateCalendarService implements GenerateCalendar {

    @Override
    public void provideHolidays(List<RecurringHoliday> recurringHolidays, List<CalendarDetail> calendarDetails, HashMap<Date, String> holiday) {
        recurringHolidays.stream().forEach((recurringHoliday) -> {
            holiday.put(recurringHoliday.getDayOfHoliday(), recurringHoliday.getDescription());
        });

        calendarDetails.stream().forEach((calendarDetail) -> {
            if (holiday.containsKey(calendarDetail.getDateOfHoliday())) {
                holiday.put(calendarDetail.getDateOfHoliday(), holiday.get(calendarDetail.getDateOfHoliday()) + ", " + calendarDetail.getDescription());
            } else {
                holiday.put(calendarDetail.getDateOfHoliday(), calendarDetail.getDescription());
            }
        });
    }

    @Override
    public void provideEvents(List<CalendarEvent> calendarEvents, HashMap<Date, CalendarEvent> eventMap) {
        calendarEvents.stream().forEach((calendarEvent) -> {
            eventMap.put(calendarEvent.getDateOfEvent(), calendarEvent);
        });
    }

    @Override
    public void provideBranchExceptionDays(List<BranchExceptionDay> branchExceptionDays, HashMap<Date, BranchExceptionDay> exceptionDayMap) {
        branchExceptionDays.stream().forEach((branchExceptionDay) -> {
            exceptionDayMap.put(branchExceptionDay.getDateOfException(), branchExceptionDay);
        });
    }

    public CalendarDayResponse getDayDetails(Calendar calendar, WeekDay weekDay, HashMap<Date, String> holidayMap, HashMap<Date, BranchExceptionDay> exceptionDayMap, HashMap<Date, CalendarEvent> eventMap) {
        Date date = calendar.getTime();
        int day = calendar.get(Calendar.DATE);
        calendar.add(Calendar.DATE, 1);
        String dayName = CalendarUtils.getDayNameOfWeek(calendar.get(Calendar.DAY_OF_WEEK));
        calendar.add(Calendar.DATE, -1);
        boolean weeklyholiday = CalendarUtils.isWeeklyHoliday(weekDay, calendar.get(Calendar.DAY_OF_WEEK));

        String branchDescription = "";
        boolean branchException = false;
        BranchExceptionDay branchExceptionDay = exceptionDayMap.get(date);
        if (branchExceptionDay != null) {
            branchDescription = branchExceptionDay.getDescription();
            branchException = branchExceptionDay.isHoliday();
        }

        String eventName = "";
        String eventDescription = "";
        CalendarEvent calendarEvent = eventMap.get(date);
        if (calendarEvent != null) {
            eventName = calendarEvent.getCalendarEventType().getName();
            eventDescription = calendarEvent.getCalendarEventType().getDescription();
        }
        calendar.add(Calendar.DATE, 1);
        return new CalendarDayResponse(dayName, day, calendar.getTime(), holidayMap.get(date) == null ? "" : holidayMap.get(date), weeklyholiday, branchDescription, branchException, eventName, eventDescription);
    }

    public void generateDays(List<CalendarMonthResponse> calendarMonthResponses, CalendarMaster calendarMaster, List<BranchExceptionDay> branchExceptionDays, Long branchId, String year) throws ParseException {
        HashMap<Date, String> holidayMap = new HashMap<>();
        provideHolidays(calendarMaster.getCalendarProfile().getRecurringHolidays(), calendarMaster.getCalendarDetails(), holidayMap);
        WeekDay weekDay = calendarMaster.getCalendarProfile().getWeekDay();
        HashMap<Date, CalendarEvent> eventMap = new HashMap<>();
        provideEvents(calendarMaster.getCalendarEvents(), eventMap);
        HashMap<Date, BranchExceptionDay> exceptionDayMap = new HashMap<>();
        provideBranchExceptionDays(branchExceptionDays, exceptionDayMap);

        int leapYear = CalendarUtils.isLeapYear(Integer.parseInt(year));
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(CalendarUtils.pareStringToDateyyyyMMdd(year + "-01-01"));
        HashSet<String> monthSet = new HashSet<>();

        CalendarMonthResponse calendarMonthResponse = null;
        List<CalendarDayResponse> calendarDayResponses = null;
        int flag = 0;
        for (int i = 1; i <= 365 + leapYear; i++) {
            String month = CalendarUtils.getMonthName(calendar.get(Calendar.MONTH));
            if (!monthSet.contains(month)) {
                if (flag == 1) {
                    calendarMonthResponse.setDays(calendarDayResponses);
                    calendarMonthResponses.add(calendarMonthResponse);
                }
                flag = 1;
                monthSet.add(month);
                calendarMonthResponse = new CalendarMonthResponse();
                calendarMonthResponse.setMonthName(month);
                calendarDayResponses = new ArrayList();
            }
            calendarDayResponses.add(getDayDetails(calendar, weekDay, holidayMap, exceptionDayMap, eventMap));
            calendar.add(Calendar.DATE, -1);
            calendar.add(Calendar.DATE, 1);
        }
        calendarMonthResponse.setDays(calendarDayResponses);
        calendarMonthResponses.add(calendarMonthResponse);

    }
}
