/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name = "week_day")
public class WeekDay implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "calendar_profile")
    private CalendarProfile calendarProfile;
    
    @Column(name = "saturday", nullable = false)
    private boolean saturday;
    
    @Column(name = "sunday", nullable = false)
    private boolean sunday;
    
    @Column(name = "monday", nullable = false)
    private boolean monday;
    
    @Column(name = "tuesday", nullable = false)
    private boolean tuesday;
    
    @Column(name = "wednesday", nullable = false)
    private boolean wednesday;
    
    @Column(name = "thursday", nullable = false)
    private boolean thursday;
    
    @Column(name = "friday", nullable = false)
    private boolean friday;

    public WeekDay() {
    }

    public WeekDay(CalendarProfile calendarProfile, boolean saturday, boolean sunday, boolean monday, boolean tuesday, boolean wednesday, boolean thursday, boolean friday) {
        this.calendarProfile = calendarProfile;
        this.saturday = saturday;
        this.sunday = sunday;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarProfile getCalendarProfile() {
        return calendarProfile;
    }

    public void setCalendarProfile(CalendarProfile calendarProfile) {
        this.calendarProfile = calendarProfile;
    }

    public boolean isSaturday() {
        return saturday;
    }

    public void setSaturday(boolean saturday) {
        this.saturday = saturday;
    }

    public boolean isSunday() {
        return sunday;
    }

    public void setSunday(boolean sunday) {
        this.sunday = sunday;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }
    
    
}
