/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import java.util.Date;

/**
 *
 * @author ProBook
 */
public class CalendarEventResponse {
    private Long id;
    private Date dateOfEvent;
    private Long eventTypeId;
    private String eventTypeName;
    private String eventTypeDescription;

    public CalendarEventResponse(Long id, Date dateOfEvent, Long eventTypeId, String eventTypeName, String eventTypeDescription) {
        this.id = id;
        this.dateOfEvent = dateOfEvent;
        this.eventTypeId = eventTypeId;
        this.eventTypeName = eventTypeName;
        this.eventTypeDescription = eventTypeDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDateOfEvent() {
        return dateOfEvent;
    }

    public void setDateOfEvent(Date dateOfEvent) {
        this.dateOfEvent = dateOfEvent;
    }

    public Long getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Long eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public String getEventTypeName() {
        return eventTypeName;
    }

    public void setEventTypeName(String eventTypeName) {
        this.eventTypeName = eventTypeName;
    }

    public String getEventTypeDescription() {
        return eventTypeDescription;
    }

    public void setEventTypeDescription(String eventTypeDescription) {
        this.eventTypeDescription = eventTypeDescription;
    }
    
}
