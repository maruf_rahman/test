/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name="branch_exception_day")
public class BranchExceptionDay implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @Column(name="branch_id")
    private Long branchId;
    
    @Temporal(TemporalType.DATE)
    @Column(name="date_of_exception")
    private Date dateOfException;
    
    @Column(name="description")
    private String description;
    
    @Column(name="holiday")
    private boolean holiday;

    public BranchExceptionDay() {
    }

    public BranchExceptionDay(Long branchId, Date dateOfException, String description, boolean holiday) {
        this.branchId = branchId;
        this.dateOfException = dateOfException;
        this.description = description;
        this.holiday = holiday;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public Date getDateOfException() {
        return dateOfException;
    }

    public void setDateOfException(Date dateOfException) {
        this.dateOfException = dateOfException;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isHoliday() {
        return holiday;
    }

    public void setHoliday(boolean holiday) {
        this.holiday = holiday;
    }
    
    
}
