/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Maruf
 */
@Entity
@Table(name = "recurring_holiday", uniqueConstraints = @UniqueConstraint(columnNames={"calendar_profile", "day_of_holiday"}))
public class RecurringHoliday implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = CalendarProfile.class)
    @JoinColumn(name = "calendar_profile")
    private CalendarProfile calendarProfile;
    
    @Temporal(TemporalType.DATE)
    @Column(name="day_of_holiday", nullable = false)
    private Date dayOfHoliday;
    
    @Column(name="description")
    private String description;

    public RecurringHoliday() {
    }

    public RecurringHoliday(CalendarProfile calendarProfile, Date dayOfHoliday, String description) {
        this.calendarProfile = calendarProfile;
        this.dayOfHoliday = dayOfHoliday;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CalendarProfile getCalendarProfile() {
        return calendarProfile;
    }

    public void setCalendarProfile(CalendarProfile calendarProfile) {
        this.calendarProfile = calendarProfile;
    }

    public Date getDayOfHoliday() {
        return dayOfHoliday;
    }

    public void setDayOfHoliday(Date dayOfHoliday) {
        this.dayOfHoliday = dayOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    
}
