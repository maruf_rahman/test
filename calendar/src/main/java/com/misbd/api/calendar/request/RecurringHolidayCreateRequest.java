/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.request;

import java.util.Date;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Maruf
 */
public class RecurringHolidayCreateRequest {
    @NotNull 
    private Long caledarProfileId;
    @NotNull
    private Date dateOfHoliday;
    private String description;

    public RecurringHolidayCreateRequest() {
    }

    public RecurringHolidayCreateRequest(Long caledarProfileId, Date dateOfHoliday, String description) {
        this.caledarProfileId = caledarProfileId;
        this.dateOfHoliday = dateOfHoliday;
        this.description = description;
    }

    public Long getCaledarProfileId() {
        return caledarProfileId;
    }

    public void setCaledarProfileId(Long caledarProfileId) {
        this.caledarProfileId = caledarProfileId;
    }

    public Date getDateOfHoliday() {
        return dateOfHoliday;
    }

    public void setDateOfHoliday(Date dateOfHoliday) {
        this.dateOfHoliday = dateOfHoliday;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
