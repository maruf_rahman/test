/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.calendar.response;

import java.util.List;

/**
 *
 * @author Maruf
 */
public class CalendarMonthResponse {
    private String monthName;
    private List<CalendarDayResponse> days;

    public CalendarMonthResponse() {
    }

    public CalendarMonthResponse(String monthName, List<CalendarDayResponse> days) {
        this.monthName = monthName;
        this.days = days;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }

    public List<CalendarDayResponse> getDays() {
        return days;
    }

    public void setDays(List<CalendarDayResponse> days) {
        this.days = days;
    }
    
    
}
