/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.common;

import com.misbd.api.calendar.entity.WeekDay;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Maruf
 */
public class CalendarUtils {

    public static Date pareStringToDateyyyyMMdd(String date) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd").parse(date);
    }

    public static int getDayName(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }

    public static int isLeapYear(int year) {
        if (year % 400 == 0) {
            return 1;
        } else if (year % 100 == 0) {
            return 0;
        } else if (year % 4 == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public static String getMonthName(int month) {
        String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        return monthNames[month];
    }

    public static String getDayNameOfWeek(int day) {
        String[] dayNames = {"Sat","Sun", "Mon", "Tue", "Wed", "Thu", "Fri"};
        return dayNames[day - 1];
    }

    public static boolean isWeeklyHoliday(WeekDay weekDay, int day) {
        boolean holiday = true;
        switch (day) {
            case 1:
                holiday = weekDay.isSaturday();
                break;
            case 2:
                holiday = weekDay.isSunday();
                break;
            case 3:
                holiday = weekDay.isMonday();
                break;
            case 4:
                holiday = weekDay.isTuesday();
                break;
            case 5:
                holiday = weekDay.isWednesday();
                break;
            case 6:
                holiday = weekDay.isThursday();
                break;
            case 7:
                holiday = weekDay.isFriday();
                break;
        }
        return !holiday;
    }
}
