/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.common;

/**
 *
 * @author Maruf
 */
public class Message {
    public static final String SAVE_MSG = "Successfully Saved";
    public static final String UPDATED_MSG = "Successfully Updated";
    public static final String DELETED_MSG = "Successfully DELETED";
    public static final String OK_MSG = "Ok";
}
