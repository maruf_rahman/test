/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.misbd.api.common;

import com.misbd.api.calendar.entity.BranchExceptionDay;
import com.misbd.api.calendar.entity.CalendarDetail;
import com.misbd.api.calendar.entity.CalendarEvent;
import com.misbd.api.calendar.entity.RecurringHoliday;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Maruf
 */
public interface GenerateCalendar {

    public void provideHolidays(List<RecurringHoliday> recurringHolidays, List<CalendarDetail> calendarDetails, HashMap<Date, String> holiday);

    public void provideEvents(List<CalendarEvent> calendarEvents, HashMap<Date, CalendarEvent> eventMap);

    public void provideBranchExceptionDays(List<BranchExceptionDay> branchExceptionDays, HashMap<Date, BranchExceptionDay> exceptionDayMap);
}
